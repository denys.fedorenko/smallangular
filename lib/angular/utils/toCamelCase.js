function toCamelCase(attr) {
  const reg = /-\w/g;

  const res = attr.replace(reg, function toUpCase(el) {
    return el.slice(1).toUpperCase();
  });

  return res;
}

export default toCamelCase;