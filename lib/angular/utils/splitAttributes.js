import toCamelCase from './toCamelCase';

function splitAttributes({ attributes }, directives) {
  const nonAngularDirectives = {};
  const angularDirectives = {};

  for (const el of attributes) {
    const { name, value } = el;
    const camelName = toCamelCase(name);

    if (directives[camelName]) {
      angularDirectives[camelName] = value;
    } else {
      nonAngularDirectives[camelName] = value;
    }
  }
  return [nonAngularDirectives, angularDirectives];
}

export default splitAttributes;