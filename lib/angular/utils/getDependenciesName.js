const getDependenciesName = fn => {
  const srtingFn = fn.toString();

  if (!(/{\n\s+('inject')/).test(srtingFn)) {
    return [];
  }

  const stringArgs = srtingFn.match(/\((.*?)\)/)[1];

  return stringArgs.split(/\s*,\s*/);
};

export default getDependenciesName;

