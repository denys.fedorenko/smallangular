export { default as getDependenciesName } from './getDependenciesName';
export { default as splitAttributes } from './splitAttributes';
export { default as toCamelCase } from './toCamelCase';
