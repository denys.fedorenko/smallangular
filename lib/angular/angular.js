import * as baseDirectives from './directives/index';
import * as baseServices from './services/index';
import * as baseFilters from './filters/index';
import { splitAttributes, getDependenciesName } from './utils/index';
import pubSub from './pubSub';

function SmallAngular() {
  const components = {};
  const configs = [];
  const controllers = {};
  const directives = { ...baseDirectives };
  const filters = { ...baseFilters };
  const $rootScope = window;
  const services = {};
  let watchers = [];

  // eslint-disable-next-line no-proto
  Object.assign($rootScope.__proto__, pubSub);

  function component(name, fn) {
    components[name] = fn;

    return this;
  }

  function config(fn) {
    configs.push(fn);

    return this;
  }

  function constant(name, value) {
    services[name] = value;

    return this;
  }

  function controller(name, fn) {
    controllers[name] = fn;

    return this;
  }

  function filter(name, fn) {
    filters[name] = fn;

    return this;
  }

  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  const getDependencies = fn =>
    getDependenciesName(fn).map(name => name === '$rootScope' ? $rootScope : services[name]);

  function service(name, fn) {
    const dependencies = getDependencies(fn);
    services[name] = fn(...dependencies);

    return this;
  }

  const compile = node => {
    const tagName = node.tagName.toLowerCase();
    const [nonAngularDirectives, angularDirectives] = splitAttributes(node, directives);
    let foundController = null;

    if (components[tagName]) {
      const dependencies = getDependencies(components[tagName]);
      const {
        controller,
        controllerAs,
        template,
        link
      } = components[tagName](...dependencies);

      if (!controller) {
        link($rootScope, node, nonAngularDirectives);
      }

      foundController = controllers[controller];

      if (controllerAs) {
        $rootScope[controllerAs] = foundController;
      }

      if (template) {
        node.innerHTML = template;
        setTimeout(() => node.querySelectorAll('*').forEach(compile));
      }

      link($rootScope, node, nonAngularDirectives, foundController);
    }

    for (const name in angularDirectives) {
      const dependencies = getDependencies(directives[name]);

      directives[name](...dependencies).link($rootScope, node, nonAngularDirectives);
    }
  };

  const parse = function(scope, str) {
    const [expressionVariable, ...filtersArray] = str.split('|').map(str => str.trim());
    const expressionResult = scope.eval(expressionVariable);

    return filtersArray.reduce((acc, filterName) => filters[filterName](acc), expressionResult);
  };

  $rootScope.$$compile = compile;
  $rootScope.$$parse = parse;

  $rootScope.$watch = (_, node, fn) => {
    watchers.push({ node, fn });
  };

  $rootScope.$apply = () => {
    watchers = watchers.filter(watcher => {
      if (watcher.node.isConnected) {
        watcher.fn(compile);
      }

      return watcher.node.isConnected;
    });
  };

  $rootScope.$applyAsync = () => setTimeout($rootScope.$apply);

  this.createApp = appName => {
    this.appName = appName;

    return this;
  };

  const initConfigs = () => configs.forEach(fn => {
    const dependencies = getDependencies(fn);
    fn(...dependencies);
  });

  const initControllers = () => {
    for (const name in controllers) {
      const Controller = controllers[name];
      const dependencies = getDependencies(Controller);
      controllers[name] = new Controller(...dependencies);
    }
  };

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    if (!node) {
      throw new Error(`No such node: ${node}`);
    }

    initConfigs();
    initControllers();
    configs.forEach(el => el());
    node.querySelectorAll('*').forEach(compile);
  };

  const initBaseServices = () => {
    for (const fn in baseServices) {
      service(fn, baseServices[fn]);
    }
  };

  this.component = component;
  this.config = config;
  this.constant = constant;
  this.controller = controller;
  this.directive = directive;
  this.filter = filter;
  this.service = service;

  initBaseServices();

  setTimeout(this.bootstrap);
}

window.angular = new SmallAngular();