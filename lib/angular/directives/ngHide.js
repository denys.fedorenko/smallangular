function ngHide() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-hide'];
      const { display } = getComputedStyle(el);

      function toggleElement() {
        el.style.display = scope.eval(value) ? 'none' : display;
      }

      toggleElement();
      scope.$watch(() => value, el, toggleElement);
    }
  };
}

export default ngHide;
