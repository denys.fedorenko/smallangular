function ngOutline() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-outline'];
      el.style.outline = `${value} solid 1px`;
      scope[value] = value;

      scope.$watch(() => value, el, () => {
        el.style.outline = scope[value] ? `${value} solid 1px` : 'none';
      });
    }
  };
}

export default ngOutline;