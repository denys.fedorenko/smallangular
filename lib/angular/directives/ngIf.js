function ngIf() {
  return {
    link(scope, el) {
      const { value } = el.attributes['ng-if'];

      function toggleElement() {
        el.hidden = !scope.eval(value);
      }

      toggleElement();
      scope.$watch(value, el, toggleElement);
    }
  };
}

export default ngIf;
