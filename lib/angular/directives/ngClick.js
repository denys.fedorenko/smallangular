function ngClick() {
  return {
    link(scope, el) {
      el.addEventListener('click', () => {
        const { value } = el.attributes['ng-click'];
        scope.eval(value);
        scope.$applyAsync();
      });
    }
  };
}

export default ngClick;