function ngRepeat() {
  return {
    link: (scope, el) => {
      const { value: expression } = el.attributes['ng-repeat'];
      const [item, iterable] = expression.split(/\s*in\s*/);

      const { parentNode, style } = el;
      const nodeCopy = el.cloneNode(true);
      style.display = 'none';

      function repeatElementAndCompile() {
        document.querySelectorAll('[ng-repeat-for-delete]').forEach(el => el.remove());
        const fragment = new DocumentFragment();

        for (const value of scope[iterable]) {
          const nodeClone = nodeCopy.cloneNode(true);
          nodeClone.setAttribute('ng-repeat-for-delete', '');
          nodeClone.removeAttribute('ng-repeat');
          scope[item] = value;

          nodeClone.innerHTML = nodeClone.innerHTML.replace(/{{(.*?)}}/g, (match, p1) =>
            scope.$$parse(scope, p1));

          fragment.appendChild(nodeClone);

          nodeClone.querySelectorAll('*').forEach(scope.$$compile);
          scope.$$compile(nodeClone);
        }

        parentNode.appendChild(fragment);
      }

      repeatElementAndCompile(scope.$$compile);
      scope.$watch(expression, el, repeatElementAndCompile);
    }
  };
}

export default ngRepeat;
