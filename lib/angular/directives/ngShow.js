function ngShow() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-show'];
      const { display } = getComputedStyle(el);

      function toggleElement() {
        el.style.display = scope.eval(value) ? display : 'none';
      }

      toggleElement();
      scope.$watch(value, el, toggleElement);
    }
  };
}

export default ngShow;
