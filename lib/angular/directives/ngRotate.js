function ngRotate() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-rotate'];
      el.addEventListener('click', () => {
        if (el.style.transform === `rotateY(${value}deg)`) {
          el.style.transform = `rotateY(${0}deg)`;
          el.style.transition = 'all 1s ease-out';
        } else {
          el.style.transform = `rotateY(${value}deg)`;
          el.style.transition = 'all 1s ease-out';
        }
      });
    }
  };
}

export default ngRotate;