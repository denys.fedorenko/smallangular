function ngInit() {
  return {
    link: (scope, node) => {
      const { value } = node.attributes['ng-init'];
      scope.eval(value);
    }
  };
}

export default ngInit;