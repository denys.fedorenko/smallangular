function ngModel() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-model'];
      scope[value] = el.value;

      if (el.type === 'checkbox') {
        scope[value] = el.checked;

        scope.$watch(() => value, el, () => {
          el.checked = scope[value];
        });
        scope.$applyAsync();

        el.addEventListener('input', e => {
          scope[value] = e.target.checked;
          scope.$applyAsync();
        });
        return;
      }

      scope.$watch(() => value, el, () => {
        el.value = scope[value];
      });

      el.addEventListener('input', e => {
        scope[value] = e.target.value;
        scope.$applyAsync();
      });

      scope.$applyAsync();
    }
  };
}

export default ngModel;