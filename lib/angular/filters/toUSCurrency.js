function toUSCurrency(string) {
  return string.replace(/\b([0-9]+)\b/g, (match, p1) => `${Number(p1).toFixed(2)} $`);
}

export default toUSCurrency;
