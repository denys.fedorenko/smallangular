function toUpperCase(string) {
  return string.toUpperCase();
}

export default toUpperCase;
