const eventsSymbol = Symbol('events');

class PubSub {
  constructor() {
    this[eventsSymbol] = {};
  }
  subscribe(name, callback) {
    if (!this[eventsSymbol][name]) {
      this[eventsSymbol][name] = [];
    }

    this[eventsSymbol][name].push(callback);
  }

  publish(name, data) {
    const event = this.[eventsSymbol][name];

    if (!event || !event.length) {
      return;
    }

    this[eventsSymbol][name].forEach(cb => cb(data));
  }
}

export default new PubSub();
