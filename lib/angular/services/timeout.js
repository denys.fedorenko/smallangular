function $timeout(rootScope) {
  'inject';

  return function(fn, timeToDelay, ...args) {
    setTimeout(() => {
      fn(...args);
      rootScope.$applyAsync();
    }, timeToDelay);
  };
}
export default $timeout;