function $http($rootScope) {
  'inject';

  function request(config) {
    const {
      fullUrl,
      method,
      data,
      transformResponse = [],
      responseType = 'json',
      headers,
      onDownloadProgress,
      onUploadProgress
    } = config;

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open(method, fullUrl);
      xhr.responseType = responseType;

      for (const key in headers) {
        xhr.setRequestHeader(key, headers[key]);
      }

      xhr.onload = function() {
        const responceObj = {
          status: xhr.status,
          responceType: xhr.responseType,
          contentType: xhr.getResponseHeader('content-type')
        };

        if (xhr.status >= 400) {
          responceObj.responce = xhr.response;
          reject(responceObj);
          return $rootScope.$applyAsync();
        }

        const transformedData = transformResponse.reduce((acc, cur) => cur(acc), xhr.response);
        responceObj.responce = transformedData ? transformedData : responceObj.responce;
        resolve(responceObj);
        $rootScope.$applyAsync();
      };

      xhr.onprogress = onDownloadProgress;
      xhr.upload.onprogress = onUploadProgress;
      xhr.onerror = () => reject(xhr);

      xhr.send(data || null);
    });
  }

  return {
    get: (fullUrl, config = {}) => request({ ...config, method: 'GET', fullUrl }),
    post: (fullUrl, data, config = {}) => request({ ...config, method: 'POST', fullUrl, data }),
    delete: (fullUrl, config = {}) => request({ ...config, method: 'DELETE', fullUrl })
  };
}

export default $http;
